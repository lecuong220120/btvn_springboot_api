package com.demo.service;

import com.demo.entities.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    Optional<Product> findProduct(Long id);
    List<Product> findAllProduct();
    Product save(Product product);
    Product update(Product product);
    void delete(Product product);
}
