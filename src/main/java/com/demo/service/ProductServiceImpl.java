package com.demo.service;

import com.demo.entities.Product;
import com.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Optional<Product> findProduct(Long id) {
        return productRepository.findById(id);
    }
    @Override
    public List<Product> findAllProduct() {
        return productRepository.findAll();
    }

    @Override
    public Product save(Product user) {
        return productRepository.save(user);
    }

    @Override
    public Product update(Product user) {
        return productRepository.save(user);
    }

    @Override
    public void delete(Product user) {
         productRepository.delete(user);
    }
}
