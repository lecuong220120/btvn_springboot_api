package com.demo.controller;

import com.demo.entities.Product;
import com.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private ProductService productService;
    @GetMapping("findProductById/{id}")
    public Product findProduct(@PathVariable Long id){
       Product find = productService.findProduct(id).get();
       return find;

    }
    @GetMapping(value = "/product")
    public List<Product> findAll(){
        return productService.findAllProduct();
    }
    @PostMapping(value="/save")
    public String save(@RequestBody Product product){
         productService.save(product);
        return "saved";
    }
    @PutMapping(value="/update/{id}")
    public String update(@PathVariable long id, @RequestBody Product product){
        Product update= productService.findProduct(id).get();
        update.setName(product.getName());
        update.setPrice(product.getPrice());
        update.setQuantity(product.getQuantity());
        productService.save(update);
        return "updated...";
    }
    @DeleteMapping(value="/delete/{id}")
    public String delete(@PathVariable long id){
        Product delete = productService.findProduct(id).get();
         productService.delete(delete);
         return "deleted";
    }
}
